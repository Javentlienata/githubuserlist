package com.javent.api.services

import com.javent.api.model.UserListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by javentira on 2019-06-19.
 */
interface GitHubService {
    @GET("search/users")
    fun listRepos(
        @Query("q") user: String,
        @Query("page") page: Int? = 1,
        @Query("per_page") perPage: Int? = 20
        ): Call<UserListResponse>
}