package com.javent.githubcermati

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.javent.api.Api
import com.javent.api.model.Item
import com.javent.api.model.UserListResponse
import com.javent.api.services.GitHubService
import com.javent.cookgram.base.BaseFullScreenActivity
import com.javent.githubcermati.viewitems.UserViewItem
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.UnsupportedEncodingException
import java.net.URLDecoder

class MainActivity : BaseFullScreenActivity() {
    private lateinit var state: MainViewModel

    private val itemAdapter = ItemAdapter<UserViewItem>()
    private val fastAdapter: FastAdapter<UserViewItem> = FastAdapter.with(itemAdapter)
    lateinit var onScrollListener: EndlessRecyclerOnScrollListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        state = ViewModelProviders.of(this).get(MainViewModel::class.java)
        setupRecyclerView()

        searchView.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            val queryDelayHandler = Handler()
            override fun onQueryTextChange(newText: String?): Boolean {
                state.query = newText ?: ""

                queryDelayHandler.removeCallbacksAndMessages(null)
                queryDelayHandler.postDelayed(Runnable {
                    reset()

                    getUsers()
                }, 500)
                return true
            }
        })
    }

    private fun reset() {
        state.pagedSearchResult.clear()
        itemAdapter.clear()
        onScrollListener.resetPageCount()
        state.nextPage = 1
        state.lastPage = 1
    }

    private fun setupRecyclerView() {
        rvUsers.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rvUsers.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        rvUsers.adapter = fastAdapter

        onScrollListener = object : EndlessRecyclerOnScrollListener(rvUsers.layoutManager, 4) {
            override fun onLoadMore(currentPage: Int) {
                if (currentPage == 0 ||
                    (state.lastPage == null && state.nextPage == null) /*end of page*/
                ) return

                getUsers()
            }
        }

        rvUsers.removeOnScrollListener(onScrollListener)
        rvUsers.addOnScrollListener(onScrollListener)
        (rvUsers.itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
    }

    fun getUsers() {
        val query = state.query
        state.errorMessage = ""

        if (query.isBlank()) {
            return
        }

        setLoading(true)
        Api.service(GitHubService::class).listRepos(query, state.nextPage)
            .enqueue(object : Callback<UserListResponse> {
                override fun onFailure(call: Call<UserListResponse>, t: Throwable) {
                    setLoading(false)
                    state.errorMessage = t.message ?: ""
                }

                override fun onResponse(
                    call: Call<UserListResponse>,
                    response: Response<UserListResponse>
                ) {
                    setLoading(false)
                    if (response.isSuccessful) {
                        response.body()?.items?.run {
                            state.pagedSearchResult.add(this)
                            renderList()
                        }

                        response.headers().get("Link")?.run {
                            GithubLinks(this).run {
                                state.nextPage = getQueryParams(next).get("page")?.first()?.toInt()
                                state.lastPage = getQueryParams(last).get("page")?.first()?.toInt()
                            }
                        } ?: run {
                            state.nextPage = null
                            state.lastPage = null
                        }
                    } else {
                        state.errorMessage = response.errorBody()?.string() ?: ""
                        setError()
                    }
                }
            })
    }

    private fun renderList() {
        val items = mutableListOf<UserViewItem>()
        state.pagedSearchResult.forEach {
            it.forEach {
                items.add(UserViewItem().apply {
                    imgUrl = it.avatarUrl
                    userName = it.login
                })
            }
        }

        itemAdapter.set(items)
    }

    private fun setLoading(isLoading: Boolean) {
        if (isLoading) {
            tvInfo.visibility = View.VISIBLE
            tvInfo.text = getString(R.string.text_loading___)
        } else {
            tvInfo.visibility = View.GONE
        }
    }

    private fun setError() {
        Toast.makeText(this, state.errorMessage, Toast.LENGTH_LONG).show()
    }

    fun getQueryParams(url: String): Map<String, List<String>> {
        try {
            val params = HashMap<String, List<String>>()
            val urlParts = url.split("\\?".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (urlParts.size > 1) {
                val query = urlParts[1]
                for (param in query.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                    val pair = param.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val key = URLDecoder.decode(pair[0], "UTF-8")
                    var value = ""
                    if (pair.size > 1) {
                        value = URLDecoder.decode(pair[1], "UTF-8")
                    }

                    var values: MutableList<String>? = params[key]?.toMutableList()
                    if (values == null) {
                        values = ArrayList()
                        params[key] = values
                    }
                    values.add(value)
                }
            }

            return params
        } catch (ex: UnsupportedEncodingException) {
            throw AssertionError(ex)
        }
    }
}

class MainViewModel : ViewModel() {
    var pagedSearchResult = mutableListOf(listOf<Item>())
    var query = ""
    var errorMessage = ""

    var nextPage: Int? = 1
    var lastPage: Int? = 1
}


