package com.javent.cookgram.base

import com.mikepenz.materialize.MaterializeBuilder

/**
 * Created by javentira on 2019-06-01.
 */
abstract class BaseFullScreenActivity : BaseActivity() {

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        setupMaterializeBuilder()
    }

    protected open fun setupMaterializeBuilder() {
        MaterializeBuilder()
            .withActivity(this)
            .withTransparentStatusBar(true).build()
    }
}