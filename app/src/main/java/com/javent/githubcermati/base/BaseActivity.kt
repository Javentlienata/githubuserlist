package com.javent.cookgram.base

import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.android.asCoroutineDispatcher
import kotlin.coroutines.CoroutineContext

/**
 * Created by javentira on 2019-06-01.
 */
internal val mainCompat = Handler(Looper.getMainLooper()).asCoroutineDispatcher("activityContext")
abstract class BaseActivity : AppCompatActivity(), CoroutineScope {
    val job: Job = SupervisorJob()
    override val coroutineContext: CoroutineContext
        get() = job + mainCompat

    override fun onDestroy() {
        job.cancel() // cancel is extension on CoroutineScope
        super.onDestroy()
    }
}