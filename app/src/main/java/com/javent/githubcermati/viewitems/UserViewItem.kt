package com.javent.githubcermati.viewitems

import android.view.View
import com.bumptech.glide.Glide
import com.javent.githubcermati.R
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import kotlinx.android.synthetic.main.item_user.view.*

class UserViewItem() : AbstractItem<UserViewItem, UserViewItem.ViewHolder>() {

    var imgUrl: String? = null
    var userName: String? = null

    //The unique ID for this type of item
    override fun getType(): Int {
        return R.id.viewitem_user
    }

    //The layout to be used for this type of item
    override fun getLayoutRes(): Int {
        return R.layout.item_user
    }

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    /**
     * our ViewHolder
     */
    class ViewHolder(view: View) : FastAdapter.ViewHolder<UserViewItem>(view) {
        val glideBuilder = Glide.with(this.itemView.context)

        override fun bindView(item: UserViewItem, payloads: List<Any>) {
            this.itemView.run {

                glideBuilder.clear(imgUserAvatar)
                glideBuilder.load(item.imgUrl).centerCrop().into(imgUserAvatar)

                tvUserName.text = item.userName
            }
        }

        override fun unbindView(item: UserViewItem) {
            glideBuilder.clear(this.itemView.imgUserAvatar)
        }
    }
}