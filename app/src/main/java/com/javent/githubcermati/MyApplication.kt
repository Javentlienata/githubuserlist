package com.javent.githubcermati

import android.app.Application
import com.facebook.stetho.Stetho

/**
 * Created by javentira on 2019-06-01.
 */
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this);
    }
}